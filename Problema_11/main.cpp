/*

Problema 11.
Este programa permite manejar lasreservas de asientos en una sala de cine,los asientos de la sala de cine están organizados en 15 ﬁlas con 20 asientos cada una.
El programa debe mostrar una representación de la sala que indique que asientos están disponibles y cuales se encuentran reservados.
Además debe permitir realizar reservas o cancelaciones al ingresar la ﬁla (letras A-O) y el número del asiento (números 1-20).
Nota: un ejemplo de visualización de una sección de la sala es el siguiente:
+ + + +
- - + +
- - - -
Donde + representa los asientos reservados y - representa los asientos disponibles.
*/
#include <iostream>

using namespace std;
//prototipo de funciones
void menu(char [15][20]);//para generar un menu
void mostrar_sillas(char[15][20]);//para imprimir las sillas disponibles
void recervar(char[15][20]);//para hacer una recerva
void quitar_recerva(char [15][20]);//para quitar una recerva

int main()
{
    char sillas[15][20];//arreglo con las pociciones de las cillas del cine
    for(int i=0;i<15;i++){//inicialmente se las coloca todas disponibles
        for(int j=0;j<20;j++)
            sillas[i][j]='-';
    }
    menu(sillas);//funcion menu
    return 0;
}

void mostrar_sillas(char sillas [15][20]){
    for(int i=0;i<15;i++){
        for(int j=0;j<20;j++)
            cout<<sillas[i][j]<<' ';
        cout<<endl;
    }
}
void recervar(char sillas[15][20]){//funcion para recervar una silla
    int fila,columna;
    cout<<"ingrece el numero de fila"<<endl;
    cin>>fila;
    cout<<"ingrece el numero de columna"<<endl;
    cin>>columna;
    if (sillas[fila][columna]=='-'){
        sillas[fila][columna]='+';
        cout<<"su silla ha cido recervada"<<endl;
    }
    else {
        cout<<"esa silla ya esta reservada porfavor intentelo nuevamente"<<endl;
    }
}
void quitar_recerva(char sillas[15][20]){//funcion para quitar las recervas
    int fila,columna;
    cout<<"ingrece el numero de fila"<<endl;
    cin>>fila;
    cout<<"ingrece el numero de columna"<<endl;
    cin>>columna;
    if (sillas[fila][columna]=='+'){
        sillas[fila][columna]='-';
        cout<<"su recerva ha cido cancela "<<endl;
    }
    else {
        cout<<"esa silla aun no ha cido reservada"<<endl;
    }

}
void menu(char sillas[15][20]){//declaracion de la funcion menu
    int numero;
    cout<<"Elija una opcion"<<endl<<endl<<"Menu"<<endl<<endl<<"1: Mostrar sillas disponible"<<endl<<"2: Hacer una recerva"<<endl<<"3: Cancelar recerva"<<endl;
    cin>>numero;
    if (numero==1){//condicional para llamar a la funvion mostrar_sillas
        while (numero==1) {
            cout<<"sillas disponibles"<<endl<<endl;
            mostrar_sillas(sillas);
            cout<<"para salir ingrece: 0"<<endl;
            cout<<"para regresar al menu ingrce: 1"<<endl;
            cin>>numero;
            if (numero==1){
                menu(sillas);
            }
            if(numero==0)
                exit(0);
        }
    }
    if(numero==2){//condicional para llamar a la funcion recervar
        while (numero==2) {
            recervar(sillas);
            cout<<"para salir ingrece: 0"<<endl;
            cout<<"para regresar al menu ingrece: 1"<<endl;
            cin>>numero;
            if (numero==1)
                menu(sillas);
            if(numero==0)
                exit(0);
        }
    }

    if(numero==3){//condicional para llamar a la funcion quitar_recerva
        while (numero==3) {
            quitar_recerva(sillas);
            cout<<endl<<"para salir ingrece: 0"<<endl;
            cout<<"para regresar al menu ingrece: 1"<<endl;
            cin>>numero;
            if (numero==1)
                menu(sillas);
            if(numero==0)
                exit(0);
        }
    }
}
