#include <iostream>

using namespace std;

/*
Problema 5.
Este programa se basa en una función que recibe un numero entero (int) y lo convierta a cadena de caracteres. Use parámetros por referencia para retornar la cadena.
Escriba un programa de prueba.
Ejemplo: si recibe un int con valor 123, la cadena que se retorne debe ser “123”.
*/

void combertir_numero_a_caracter(char *x, int i, int num);

int main()
{
    int numero,tamano,contador=0;
    cout<<"Ingrece un numero"<<endl;
    cin >>numero;
    tamano=numero;

    while (contador<tamano) {//Ciclo para determinar la cantidad de dijitos de un numero
        tamano=tamano/10;
        contador++;
        }

    //cout<<contador<<endl;
    char arreglo[100];// Arreglo cuyo tamaño es el numero de dijitos del numero ingresado


    combertir_numero_a_caracter(arreglo,contador,numero);//Llamando la funcion

    cout<<arreglo<<endl;

    return 0;
}


void combertir_numero_a_caracter(char *x,int i,int num){//Implementacion de la funcion, recibe un puntero a un arreglo

    for(int k=i;k>=0;k--){      //Recibe el numero de digitos del numero ingresado y el numero ingresado
        x[k]=(num%10)+48;   // Cado cada digito y lo voy guardando de atras para adelante, sumandole 48 para convertirlo en caracteres y poderlos guardar en un arreglo de char
        num/=10;

    }
}
