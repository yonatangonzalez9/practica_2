#include <iostream>
using namespace std;

/*  Este programa  permite hallar la intersección entre un par de rectángulos. Representando
los rectángulos como arreglos de 4 datos de la siguiente manera:
Los primeros 2 datos corresponden a las coordenadas de la esquina superior izquierda del rectángulo (x,y)
como se observa en la Figura 5.
Los siguientes 2 datos representan el ancho y la altura del rectángulo
*/

int coordenada(int*a,int*b,int c, int d,int *e);
void anchura(int a[4], int *w);
void altura(int a[4],int *w);


int main()
{
    int rectanguloA[4]={0,0,8,4}, rectanguloB[4]={5,2,6,7};

    return 0;
}
