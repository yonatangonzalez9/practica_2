#include <iostream>

using namespace std;

/* Este programa recibe una cantidad de dinero y devuelve el número de billetes
y monedas de cada denominación para completar la cantidad deseada. Si por medio de los billetes y monedas
disponibles no se puede lograr la cantidad deseada, el sistema deberá decir lo que resta para lograrla. Use arreglos
y ciclos para realizar el programa.
*/

int main()
{
    int cant_dinero, var_auxiliar=0;
    int billetes[10]={50000,20000,10000,5000,2000,1000,500,200,100,50};
    int cantidad_de_billetes[10]={0,0,0,0,0,0,0,0,0,0};

    cout << "Ingrese la cantidad de dinero: " << endl;
    cin >> cant_dinero;

    while(cant_dinero!=0){

        if(cant_dinero/billetes[var_auxiliar] != 0){ //Aqui vamos mirando si la cantidad ingresada es divisible por cada denominacion de billetes
            cantidad_de_billetes[var_auxiliar]=cant_dinero/billetes[var_auxiliar];// Si lo es, vamos guardando la cantidad de billestes que hay
            cant_dinero = cant_dinero % billetes[var_auxiliar]; //Aqui ya reducimos la cantidad quitando los billetes que ya contamos
        }

        else{
            cant_dinero = cant_dinero % billetes[var_auxiliar]; //Sino lo es, de una reducimos la cantidad
            cantidad_de_billetes[var_auxiliar]=0; //Y en su posicion ponemos cero cantidad de esos billetes
        }
        if(var_auxiliar==10) break; //Si la variable sobrepasa la cantidad de repeticiones (10 por ser la cantidad de denominaciones de billetes) terminamos el ciclo
        var_auxiliar++;
    }

    for(int i=0;i<10;i++){
        cout<<billetes[i]<<": "<<cantidad_de_billetes[i]<<endl; //Imprimimos la cadena final
    }
    cout<<"Faltante: "<<cant_dinero<<endl;

    return 0;
}
