/*
Problema 9.
Este programa  recibe un número n y lee una cadena de caracteres numéricos, el programa  separa la cadena de caracteres en números de n cifras, los suma e imprime el resultado.
En caso de no poderse dividir exactamente en números de n cifras se colocan ceros a la izquierda del primer número.
*/

#include <iostream>
using namespace std;

void poten(int,int&);

int main()
{
    int contador=0;
    int total_sumado=0;
    int numero_particiones;
    int tama_array=0;
    int indice=0;
    int base;
    int suma;
    cout << "Ingrese el numero de particiones N que quiere: "<<endl;
    cin>> numero_particiones;
    int array[100]={8,7,5,1,2,3,9,5};

    int ceros=0;
    int salir=0;

    for(int i=0;array[i];i++){ //Obtengo el tamaño de la cadena
            tama_array++;
        }
    int tama_array2 = tama_array;



    while (tama_array%numero_particiones!=0){ //Obtengo cuantos ceros voy a tener que agregar
        tama_array+=1;
        ceros=ceros+1;
    }

    int particiones_arreglo = tama_array/numero_particiones;
    salir =(tama_array/particiones_arreglo)-ceros;
    int numero2=salir-1;

    for (int i=0;i<particiones_arreglo;i++){
        suma=0;
        for (int n=0;n<numero_particiones;n++){
            poten(numero2,base);
            suma=array[indice]*base+suma; //Saco el numero y lo multiplico por la base correspondiente
            indice=indice+1; //Voy moviendome en el arreglo
            numero2=numero2-1;
            contador=contador+1;
            if (contador==salir){
                numero2=numero_particiones-1;
                break;
            }

        }
        numero2=numero_particiones-1;
        total_sumado+=suma;

    }
    cout<<"Original: ";
    for (int i=0;i<tama_array2;i++){
        cout<<array[i];
    }
    cout<<'.'<<endl<<"Suma: "<<total_sumado<<'.'<<endl;

    return 0;
}

void poten(int numero,int& base){ //Esta funcion es para saber y hallar en que base va cada numero que voy sacando del arreglo
    base=1;
    for (int i=0;i<numero;i++){
        base=base*10;
    }
}
