#include <iostream>

using namespace std;

/*Este programa se basa en una función que compara 2 cadenas de caracteres y retorna un valor lógico verdadero si son
iguales y falso en caso contrario, no olvide también tener en cuenta la longitud de las cadenas
*/

bool tama_cadena(char *cadena1, char *cadena2);

int main()
{

    char cadena1[100];
    char cadena2[100];

    cout<<"Ingrese la primera frase: "<<endl;
    cin>>cadena1;

    cout<<"Ingrese la segunda frase: "<<endl;
    cin>>cadena2;

    cout<<tama_cadena(cadena1,cadena2)<<endl;

    return 0;
}


bool tama_cadena(char *cadena1, char *cadena2){ //Recibe dos punteros a las cadenas ingresadas
    int conta_tama1=0,conta_tama2=0;
    int iguales=0;

    for(int i=0; cadena1[i];i++){ //Mira el tamaño de la cadena
        conta_tama1++;
    }
    for(int i=0; cadena2[i];i++){
        conta_tama2++;
    }

    if(conta_tama1 == conta_tama2){ //Si son de igual tamaño

        for(int i=0; i<conta_tama1;i++){ //Verifica que sean iguales caracter por caracter y cuenta
            if(cadena1[i]== cadena2[i]) iguales++;
        }
    }

    if(iguales == conta_tama1) return true; //Si la cantidad de caracteres son iguales al tamaño de caulquiera, entonces son iguales

    else return false;

}
