#include <iostream>
using namespace std;
int suma(int N,int S);

/* Dos números a y b (a != b) son amigables si la suma de los divisores de a (excluyéndose el mismo)
es igual a b, y viceversa. Ej: los divisores de 220 son 1, 2, 4, 5, 10, 11, 20, 22, 44, 55 y 110; y suman 284. Los divisores
de 284 son 1, 2, 4, 71 y 142; y suman 220. Entonces 220 y 284 son amigables. Este programa recibe un
número y halla la suma de todos los números amigables menores al número ingresado.
*/

int main(){


    int numero_ingresado,posible_numero_amigable1,posible_numero_amigable2;
    int sum1=1,sum=0;

    cout<<"ingrese un numero"<<endl;
    cin>>numero_ingresado;

    if(numero_ingresado<220){
        cout<<"Este numero no es amigable"<<endl;
    }
    else{
        for(int i=220;i<=numero_ingresado;i++){ //Se empieza el ciclo desde 220 por ser el menor numero amigable

            posible_numero_amigable1=suma(i,sum1); //Se encuentra su posible numero amigable
            posible_numero_amigable2=suma(numero_ingresado,sum1);

            if((posible_numero_amigable2==i) && (i!=posible_numero_amigable1) ){ //(posible_numero_amigable2==i) verifica que sean amigable;(i!=posible_numero_amigable1) se verifica que no sea el mismo nummero

                sum+=i;//Se van sumando los numeros amigables

            }

        }
        cout<<"La suma de los numeros amigables es "<<sum<<endl;
    }



   return 0;
}

int suma(int N,int S){   /*Esta funcion nos calcula la suma de los divisores del numero ingresado
                            y la retorna*/
    for(int i=2;i<N;i++){
            if(N % i==0){
                    S=S+i;
            }
    }
    return S;
}
